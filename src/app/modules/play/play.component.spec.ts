import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayComponent } from './play.component';
import { of } from 'rxjs';
import { QuestionsService } from 'src/app/core/services/questions.service';

describe('PlayComponent', () => {
  let component: PlayComponent;
  let fixture: ComponentFixture<PlayComponent>;
  let questionsService: QuestionsService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PlayComponent],
    }).compileComponents();

    questionsService = TestBed.inject(QuestionsService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
