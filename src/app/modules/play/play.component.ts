import { Component, OnInit } from '@angular/core';
import { QuestionsService } from '../../core/services/questions.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { QuestionModel } from '../../core/state/question.model';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss'],
})
export class PlayComponent implements OnInit {
  questions$ = this.questionsService.questions$;
  gettingQuestions$ = this.questionsService.gettingQuestions$;
  getQuestionsSubscription: Subscription = this.route.queryParams
    .pipe(
      switchMap((params) =>
        this.questionsService.getQuestions({
          type: params.type,
          amount: params.amount,
          difficulty: params.difficulty,
        })
      )
    )
    .subscribe({
      error: (err) => {
        this.error = "We couldn't get the questions."
      },
    });

  error: string = '';
  finish: boolean = false;
  correct: number = 0;
  incorrect: number = 0;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly questionsService: QuestionsService
  ) {}

  ngOnInit(): void {
    this.questions$.subscribe({
      next: (value) => {
        if (value.length) {
          this.finish = value.every((question) => question.selectedId);

          if (this.finish) {
            this.correct = value.filter(
              (question) => question.selectedId === question.correctId
            ).length;
            this.incorrect = value.length - this.correct;
          }
        }
      },
    });
  }

  onAnswerClicked(
    questionId: QuestionModel['_id'],
    answerSelected: string
  ): void {
    this.questionsService.selectAnswer(questionId, answerSelected);
  }
}
